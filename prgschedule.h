/*
 Copyright 2016-2019 - Domenico Ferrari <domfe@tiscali.it>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __PRGSCHEDULE_H__
#define __PRGSCHEDULE_H__

#ifdef DEBUG_OFFLINE
  typedef unsigned char byte;
#endif

#define Monday    0x40
#define Tuesday   0x20
#define Wednesday 0x10
#define Thursday  0x08
#define Friday    0x04
#define Saturday  0x02
#define Sunday    0x01

class PrgSchedule
{
  public:
    PrgSchedule() {}

  class Relay
  {
    public:
      typedef struct {
        bool active;
        byte starth;
        byte startm;
        byte endh;
        byte endm;
        byte dayofweek;
      } ProgramType;

    public:
      Relay() {}

      static const byte programcount=5;
      ProgramType program[programcount];
  };

  static const byte relaycount=1;
  Relay relay[relaycount];
};



#endif
