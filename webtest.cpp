/*
 Copyright 2016 - Domenico Ferrari <domfe@tiscali.it>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __xtensa__

#include <iostream>
#include <fstream>
#include <string>
#include "web.h"
#include "prgschedule.h"

using namespace std;
#define String std::string

PrgSchedule ScheduleData;

static void InitProgramData()
{
  int i;
  int j;
  PrgSchedule::Relay::ProgramType *program;

  for(i=0; i<ScheduleData.relaycount; i++)
  {
    PrgSchedule::Relay* r;
    r=&ScheduleData.relay[i];
    for(j=0; j<r->programcount; j++)
    {
      program=&r->program[j];
      program->starth=i;
      program->startm=32+i;
      program->endh=j;
      program->endm=44+j;
      program->active=j % 2;
      program->dayofweek=Tuesday | Monday;
    }
  }
}

int main(int argc, char *argv[])
{
  bool rs;
  String page;
  MqttConfig mc;
  StaticIPConfig staticip;
  ofstream out;

  page.reserve(8000);

  InitProgramData();

  SetAppName("app name");

  SetSensor(22.7, "last read time");

  rs=true;
  mc.server="localhost";
  mc.port=1883;
  mc.username="user";
  mc.password="pass";
  mc.toplevel="myhome/daquarium";

  staticip.ip="192.168.1.123";
  staticip.dns="192.168.1.254";
  staticip.gw="192.168.1.1";
  staticip.subnet="255.255.255.0";

  page.clear();
  CommandPage(&page);
  out.open("command.html");
  out << page;
  out.close();

  page.clear();
  ProgramPage(&page, &ScheduleData);
  out.open("program.html");
  out << page;
  out.close();

  page.clear();
  ConfigPage(&page, "app name", "ntpserver", "apikey", true, staticip, mc, "testo errore");
  out.open("config.html");
  out << page;
  out.close();

  page.clear();
  AboutPage(&page, String(__DATE__), String(__TIME__), "STARTTIME", "CHIPID", 100, "reset\nreset\nreset reason", "apname", -62);
  out.open("about.html");
  out << page;
  out.close();

  page.clear();
  StatusPage(&page, String("time date"), String("24.0"), String("0.3"), rs, &ScheduleData);
  out.open("status.html");
  out << page;
  out.close();

  page.clear();
  SetPointPage(&page, String("24.0"), String("0.3"));
  out.open("setpoint.html");
  out << page;
  out.close();

  page.clear();
  int n=2;
  String network[n]={"net1", "net 2"};
  WiFiPage(&page, String("access point"), String("wifisecret"), network, n);
  out.open("wifi.html");
  out << page;
  out.close();

  page.clear();
  RefreshPage(&page, 5, String("Restarting message..."), String("newurl"));
  out.open("refresh.html");
  out << page;
  out.close();
}

#endif
