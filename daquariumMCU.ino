/*
 Copyright 2017 - Domenico Ferrari <domfe@tiscali.it>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <time.h>
#include <DNSServer.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266mDNS.h>
#include <ESP8266httpUpdate.h>
#include <ESP8266HTTPUpdateServer.h>
#include <Ticker.h>
#include <FS.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <StreamString.h>

#define ARDUINOJSON_USE_LONG_LONG 1 // we need to store long long
#include <ArduinoJson.h>
/*

 StaticJsonDocument<capacity> doc;
 DynamicJsonDocument doc(capacity);

 StaticJsonDocument allocates document on the stack.
 DynamicJsonDocument allocates document on the heap.

 ArduinoJson provides macros for computing precisely the capacity of the JsonDocument.
 The macro to compute the size of an object is JSON_OBJECT_SIZE(). (JSON_ARRAY_SIZE(2))
 It takes one argu-ment: the number of members in the object.

 A read-only input requires a higher capacity.
 When the argument passed to deserializeJson() is of typechar*orchar[], ArduinoJson 
 uses a mode called “zero-copy.” It has this name because the parser never makes any
 copy of the input; instead, it stores pointers pointing inside the input buffer.
 In the zero-copy mode, when a program requests the content of a string member,
 ArduinoJson returns a pointer to the beginning of the string in the input buffer. To 
 make it possible, ArduinoJson inserts null-terminators at the end of each string; it is
 the reason why this mode requires the input to be writable.

 In practice, you would not use the exact length of the strings because it’s safer to add
 a bit of slack, in case the input changes. My advice is to add 10% to the longest
 possible string, which gives a reasonable margin.

 https://arduinojson.org/v6/assistant/

*/

#include <PubSubClient.h>
#include <EEPROM.h>
#include "web.h"

const time_t ValidTime=60*60*24*365*40; // start roughly from 2010      

// DNS server
const byte DNS_PORT = 53;
DNSServer dnsServer;

ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;
const IPAddress apIP(192, 168, 4, 1);
const String webUsername="admin";
String sysPassword="password";
String appName="DAquarium";
String staticIP="";
String staticDNS="";
String staticGW="";
String staticSubnet="";
bool recoveryCheck=true;
String mqttServer="";
int mqttPort=1883;
String mqttUsername="";
String mqttPassword="";
String mqttToplevel="daquarium";
bool invertedLogic=false;
long mqttLastReconnectAttempt = 0;
bool mqttEnabled=false;
String Hostname;

uint32_t subnetMask=0; // used for checking if the web client is in the same subnet

WiFiClient MqttTestClient; // client for testing Mqtt connection
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

// Central Europe Timezone
#define TZ "CET-1CEST,M3.5.0,M10.5.0/3"
#define SECS_PER_MIN 60
#define SECS_PER_HOUR 3600
#define SECS_PER_DAY 86400

String timeServer;
const String timeServerDefault = "time.nist.gov"; // time.nist.gov NTP server
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
String apiKey;

DeviceAddress DS18B20addr;

const String configFilePath="/config.conf";
const String programDatafilePath="/programdata.conf";

PrgSchedule ScheduleData;

bool standaloneMode;

String startTime;
HTTPClient httpClient;

const int UPDATE_INTERVAL_SECS = 2 * SECS_PER_MIN;
const int ALARM_CHECK_SECS = SECS_PER_MIN / 2; // check two times per minute

#define TEMPERATURE_PRECISION 12

volatile bool readyForUpdate=false;
volatile bool readyForAlarmHandle=false;
volatile bool readyForDataSend=false;
volatile bool readyForPowerOff=false;

Ticker sendTicker;
Ticker ntpTicker;
Ticker sensorTicker;
Ticker alarmTicker;
Ticker offTicker;

float waterTermperature;
float setPointValue=24.0;
float setPointRange=0.2;

// RED=Vcc
// BLACK=GND
// BLUE=Signal
// pull-up resistor 4.7kOhm
OneWire oneWire(D2);
DallasTemperature DS18B20(&oneWire);

#ifdef ESP8266_WEMOS_D1MINI
  #pragma message "Compiling for D1 mini"
#endif
const int lampPin=D1;
bool lampOn=false; // actual state of the relay

const int heaterPin=D3;
bool heaterOn=false; // actual state of the relay

int ledPin=LED_BUILTIN;

String formatTime(time_t t)
{
  char s[20];

  strftime(s, sizeof(s), "%H:%M:%S %d/%m/%Y", localtime(&t));
  return s;
}

// send an NTP request to the time server at the given address
void readNTPtime()
{
  configTime(0, 0, timeServer.c_str());
  setenv("TZ", TZ, 1);
  tzset();
  return;
}

void WebRefresh(String* page)
{
  //server.client().setNoDelay(true);
  server.send(200, "text/html", *page);
  delay(100);
  server.client().stop();
}

void handleStatus()
{
  String tm;
  String page;

  Serial.println(F("handleStatus"));
  tm=formatTime(time(nullptr));

  StatusPage(&page, tm, String(setPointValue, 2), String(setPointRange, 2), lampOn, &ScheduleData);

  server.send(200, F("text/html"), page);
}

bool checkProgramChanged(int p, int r, PrgSchedule::Relay::ProgramType *program)
{
  bool changed=false;
  String base;
  byte dayofweek=0;
  byte starth;
  byte startm;
  byte endh;
  byte endm;
  byte active;

  base=F("p");
  base+=p;
  base+=F("r");
  base+=r;

  // check if data is available
  if(!server.hasArg(base))
    return false;  // data is not sent, so is not changed

  starth=server.arg(base + F("starth")).toInt();
  startm=server.arg(base + F("startm")).toInt();
  endh=server.arg(base + F("endh")).toInt();
  endm=server.arg(base + F("endm")).toInt();
  active=server.hasArg(base + F("active"));

  if(starth!=program->starth)
  {
    program->starth=starth;
    changed=true;
  }

  if(startm!=program->startm)
  {
    program->startm=startm;
    changed=true;
  }

  if(endh!=program->endh)
  {
    program->endh=endh;
    changed=true;
  }

  if(endm!=program->endm)
  {
    program->endm=endm;
    changed=true;
  }

  if(active!=program->active)
  {
    program->active=active;
    changed=true;
  }
  
  if(server.hasArg(base + F("day1")))
    dayofweek|=Monday;
    
  if(server.hasArg(base + F("day2")))
    dayofweek|=Tuesday;
    
  if(server.hasArg(base + F("day3")))
    dayofweek|=Wednesday;
    
  if(server.hasArg(base + F("day4")))
    dayofweek|=Thursday;
    
  if(server.hasArg(base + F("day5")))
    dayofweek|=Friday;
    
  if(server.hasArg(base + F("day6")))
    dayofweek|=Saturday;
    
  if(server.hasArg(base + F("day7")))
    dayofweek|=Sunday;
    
  if(dayofweek!=program->dayofweek)
  {
    program->dayofweek=dayofweek;
    changed=true;
  }

  return changed;
}

void handleProgram()
{
  bool changed=false;
  PrgSchedule::Relay* r;
  String page;
  PrgSchedule::Relay::ProgramType *program;


  Serial.println(F("handleProgram"));
  if(server.hasArg("ok"))
  {
    for(int i=0; i<ScheduleData.relaycount; i++)
    {
      r=&ScheduleData.relay[i];
      for(int j=0; j<r->programcount; j++)
      {
        program=&r->program[j];
        if(checkProgramChanged(j, i, program))
          changed=true;
      }
    }

    // if something has changed then save to permanent storage
    if(changed)
      saveProgramData();
  }

  {
    int p;

    p=server.arg(F("program")).toInt();
    if(p==1)
      ProgramPage(&page, &ScheduleData);
  }

  server.send(200, F("text/html"), page);
}

void handleCommand()
{
  String page;

  Serial.println(F("handleCommand"));
  if(server.hasArg("ok"))
  {
    // form was submitted, take action
    String r1radio=server.arg("r1radio");
    int m;

    if(r1radio=="r1on")
    {
      lampPowerOn();
      m=server.arg("r1time").toInt();
      offTicker.once(m*60, setReadyForPowerOff);
    }
    else if(r1radio=="r1off")
      lampPowerOff();
  }

  CommandPage(&page);
  server.send(200, "text/html", page);
}

void handleSetPoint()
{
  String page;

  Serial.println(F("handleSetPoint"));
  if(server.hasArg("ok"))
  {
    setPointValue=server.arg("value").toFloat();
    setPointRange=server.arg("range").toFloat();

    saveConfig();
  }

  SetPointPage(&page, String(setPointValue, 2), String(setPointRange, 2));
  server.send(200, "text/html", page);
}

void handleReset()
{
  String page;

  Serial.println(F("handleReset"));
  SPIFFS.remove(programDatafilePath);

  RefreshPage(&page, 10, F("Restarting..."), "/");
  WebRefresh(&page);
  ESP.restart();
}

void handleReboot()
{
  Serial.println(F("handleReboot"));
  String page;

  RefreshPage(&page, 10, F("Restarting..."), "/");
  WebRefresh(&page);
  ESP.restart();
}

void handleAbout()
{
  String page;

  Serial.println(F("handleAbout"));
  StreamString version;
  
  version.printf("Version %s Compiled on %s %s", PRJ_VERSION, __DATE__, __TIME__);
  AboutPage(&page, version, startTime, String(ESP.getChipId(), HEX), ESP.getCpuFreqMHz(), ESP.getResetReason(), WiFi.SSID(), WiFi.RSSI(), ARDUINO_BOARD_ID);
  server.send(200, "text/html", page);
}

void handleSyncTime()
{
  String page;
  String message;
  MqttConfig mc;
  StaticIPConfig staticip;

  readNTPtime();

  staticip.ip=staticIP;
  staticip.dns=staticDNS;
  staticip.gw=staticGW;
  staticip.subnet=staticSubnet;

  mc.server=mqttServer;
  mc.port=mqttPort;
  mc.username=mqttUsername;
  mc.password=mqttPassword;
  mc.toplevel=mqttToplevel;

  message=String(F("Time synced: ")) + formatTime(time(nullptr));
  handleAlarms(true);
  ConfigPage(&page, appName, timeServer, apiKey, invertedLogic, staticip, mc, message);
  server.send(200, "text/html", page);
}

void handleWiFi()
{
  String page;

  if(server.hasArg("ok"))
  {
    RefreshPage(&page, 12, F("Restarting..."), "/");
    WebRefresh(&page);

    WiFi.persistent(true);
    WiFi.disconnect();
    WiFi.begin(server.arg("ssid").c_str(), server.arg("password").c_str());
    for(int i=0; i<20 && WiFi.status()!=WL_CONNECTED; i++)
      delay(500);
   
    ESP.restart();
  }
  else if(server.hasArg("wps"))
  {
    RefreshPage(&page, 12, F("Restarting..."), "/");
    WebRefresh(&page);

    WiFi.mode(WIFI_STA); // WPS only works in station mode
    delay(1000);
    WiFi.persistent(true);
    WiFi.beginWPSConfig();
    for(int i=0; i<20 && WiFi.status()!=WL_CONNECTED; i++)
      delay(500);

    ESP.restart();
  }
  else if(server.hasArg("disconnect"))
  {
    RefreshPage(&page, 10, F("Restarting..."), String("http://")+apIP.toString());
    WebRefresh(&page);

    WiFi.persistent(true);
    WiFi.disconnect();
    delay(100);
    // start the board in AP mode
    ESP.restart();
  }

  int n=WiFi.scanNetworks();
  const int maxnet=10;
  String network[maxnet];
  if(n>0)
  {
    n=(n>maxnet ? maxnet: n);
    for(int i=0; i<n && i<maxnet; i++)
      network[i]=WiFi.SSID(i);
  }

  WiFiPage(&page, WiFi.SSID(), WiFi.psk(), network, n);
  server.send(200, "text/html", page);
}

void HandleRoot()
{
  Serial.println(F("HandleRoot"));
  uint32_t local=server.client().localIP();
  uint32_t remote=server.client().remoteIP();
  bool authreq=true;

  authreq=((local & subnetMask) != (remote & subnetMask)); // request authentication if client is not in the subnet of the server
  authreq&=(sysPassword.length()>0);

  if(authreq)
  {
    if(!server.authenticate(webUsername.c_str(), sysPassword.c_str()))
      return server.requestAuthentication();
  }

  Serial.println(F("Serving request..."));

  if(server.hasArg("program"))
    handleProgram();
  else if(server.hasArg("command"))
    handleCommand();
  else if(server.hasArg("config"))
    handleConfig();
  else if(server.hasArg("setpoint"))
    handleSetPoint();
  else if(server.hasArg("reset"))
    handleReset();
  else if(server.hasArg("reboot"))
    handleReboot();
  else if(server.hasArg("synctime"))
    handleSyncTime();
  else if(server.hasArg("download"))
    downloadData();
  else if(server.hasArg("about"))
    handleAbout();
  else if(server.hasArg("wifi"))
    handleWiFi();
  else
    handleStatus();
  Serial.println(F("...page done!"));
}

void HandleNotFound()
{
  Serial.println(F("HandleNotFound"));
  String message = F("File Not Found\n\n");
  message += F("URI: ");
  message += server.uri();
  message += F("\nMethod: ");
  message += (server.method() == HTTP_GET) ? F("GET"): F("POST");
  message += F("\nArguments: ");
  message += server.args();
  message += F("\n");
  for (uint8_t i=0; i<server.args(); i++)
  {
    message+=F(" ");
    message+=server.argName(i);
    message+=F(": ");
    message+=server.arg(i);
    message+=F("\n");
  }

  server.send(404, F("text/plain"), message);
}

void initProgramData()
{
  int i;
  int j;
  PrgSchedule::Relay::ProgramType *program;

  for(i=0; i<ScheduleData.relaycount; i++)
  {
    PrgSchedule::Relay* r;
    r=&ScheduleData.relay[i];
    for(j=0; j<r->programcount; j++)
    {
      program=&r->program[j];
      program->active=0;
      program->starth=0;
      program->startm=0;
      program->endh=0;
      program->endm=0;
      program->dayofweek=0;
    }
  }
}

void saveProgramData()
{
  PrgSchedule::Relay::ProgramType *program;
  int i, j;
  File f;
  const size_t bufsize = JSON_OBJECT_SIZE(2) + // relaycount + relay
                         JSON_ARRAY_SIZE(1) + // relay[]
                         JSON_OBJECT_SIZE(2) + // programcount + program
                         JSON_ARRAY_SIZE(5) + // program[]
                         5*JSON_OBJECT_SIZE(6); // program
  DynamicJsonDocument doc(bufsize);
  doc["relaycount"] = (int) ScheduleData.relaycount;
  JsonArray relay = doc.createNestedArray("relay");

  for(i=0; i<ScheduleData.relaycount; i++)
  {
    PrgSchedule::Relay* r;
    r=&ScheduleData.relay[i];
    JsonObject relay_i = relay.createNestedObject();
    relay_i["programcount"]=(int) r->programcount;
    JsonArray relay_i_program = relay_i.createNestedArray("program");
    for(j=0; j<r->programcount; j++)
    {
      JsonObject relay_i_program_i = relay_i_program.createNestedObject();
      program=&r->program[j];
      relay_i_program_i["active"] = program->active;
      relay_i_program_i["starth"] = program->starth;
      relay_i_program_i["startm"] = program->startm;
      relay_i_program_i["endh"] = program->endh;
      relay_i_program_i["endm"] = program->endm;
      relay_i_program_i["dayofweek"] = program->dayofweek;
    } 
  }

  f=SPIFFS.open(programDatafilePath, "w");
  if(!f)
    Serial.println(F("Error opening config file for program data"));
  else
  {
    serializeJsonPretty(doc, f);

    f.close();
  }
}

void loadProgramData()
{
  PrgSchedule::Relay::ProgramType *program;
  bool ok=true;
  File f;

  f=SPIFFS.open(programDatafilePath, "r");
  if(!f)
    ok=false;

  if(ok)
  {
    const size_t bufsize = JSON_OBJECT_SIZE(2) + // relaycount + relay
                           JSON_ARRAY_SIZE(1) + // relay[]
                           JSON_OBJECT_SIZE(2) + // programcount + program
                           JSON_ARRAY_SIZE(5) + // program[]
                           5*JSON_OBJECT_SIZE(6) + // program
                           f.size();
    DynamicJsonDocument doc(bufsize);
    DeserializationError error = deserializeJson(doc, f);
    if(!error)
    {
      // it's fixed, there's only one relay
      // ScheduleData.relaycount=root["relaycount"];
      // ScheduleData.relay=new Schedule::Relay[ScheduleData.relaycount];
      for(int i=0; i<ScheduleData.relaycount; i++)
      {
        PrgSchedule::Relay* r;
        r=&ScheduleData.relay[i];
        // it's fixed... for now
        // r->programcount=doc["relay"][i]["programcount"];
        // r->program=new PrgSchedule::Relay::Program[r->programcount];
        for(int j=0; j<r->programcount; j++)
        {
          JsonObject relay_program = doc["relay"][i]["program"][j];

          program=&r->program[j];
          program->active = relay_program["active"];
          program->starth = relay_program["starth"];
          program->startm = relay_program["startm"];
          program->endh = relay_program["endh"];
          program->endm = relay_program["endm"];
          program->dayofweek = relay_program["dayofweek"];
        } // for programcount
      } // for relaycount
    }
    else
      ok=false;
  }

  if(f)
    f.close();

  if(!ok)
  {
    initProgramData();
    saveProgramData();
  }
}

void sendFile(String path)
{
  File file = SPIFFS.open(path, "r");
  server.streamFile(file, "application/octet-stream");
  file.close();
}

void downloadData()
{
  int p;

  p=server.arg(F("download")).toInt();
  switch(p)
  {
    case 1: sendFile(configFilePath); break;
    default: sendFile(programDatafilePath); break;
  }
}

void publishPowerStatus(bool on)
{
  String buf;
  const size_t bufsize = JSON_OBJECT_SIZE(2);
  StaticJsonDocument<bufsize> doc;
  String connTopic=mqttToplevel + "/status/active";

  time_t ts=time(nullptr);
  doc["val"] = (on ? 1: 0);
  doc["ts"] = ts*1000LL; // millis
  serializeJson(doc, buf);
  mqttClient.publish(connTopic.c_str(), buf.c_str());
}

void publishHeaterStatus(bool on)
{
  String buf;
  const size_t bufsize = JSON_OBJECT_SIZE(2);
  StaticJsonDocument<bufsize> doc;
  String connTopic=mqttToplevel + "/heater/status/active";

  time_t ts=time(nullptr);
  doc["val"] = (on ? 1: 0);
  doc["ts"] = ts*1000LL; // millis
  serializeJson(doc, buf);
  mqttClient.publish(connTopic.c_str(), buf.c_str());
}

void heaterPowerOn()
{
  digitalWrite(heaterPin, (invertedLogic ? LOW: HIGH));
  heaterOn=true;

  if(mqttEnabled)
    publishHeaterStatus(heaterOn);
}

void heaterPowerOff()
{
  digitalWrite(heaterPin, (invertedLogic ? HIGH: LOW));
  heaterOn=false;

  if(mqttEnabled)
    publishHeaterStatus(heaterOn);
}

void lampPowerOn()
{
  digitalWrite(lampPin, (invertedLogic ? LOW: HIGH));
  lampOn=true;

  if(mqttEnabled)
    publishPowerStatus(lampOn);
}

void lampPowerOff()
{
  digitalWrite(lampPin, (invertedLogic ? HIGH: LOW));
  lampOn=false;

  if(mqttEnabled)
    publishPowerStatus(lampOn);
}

#define dowSunday     0
#define dowMonday     1
#define dowTuesday    2
#define dowWednesday  3
#define dowThursday   4
#define dowFriday     5
#define dowSaturday   6

bool validDay(int wday, int dayofweek)
{
  bool valid=false;

  valid=valid || ((dayofweek & Monday) && wday==dowMonday);
  valid=valid || ((dayofweek & Tuesday) && wday==dowTuesday);
  valid=valid || ((dayofweek & Wednesday) && wday==dowWednesday);
  valid=valid || ((dayofweek & Thursday) && wday==dowThursday);
  valid=valid || ((dayofweek & Friday) && wday==dowFriday);
  valid=valid || ((dayofweek & Saturday) && wday==dowSaturday);
  valid=valid || ((dayofweek & Sunday) && wday==dowSunday);

  return valid;
}

void handleAlarms(bool firstTime)
{
  struct tm *t;
  PrgSchedule::Relay::ProgramType *program;
  time_t start_t;
  time_t end_t;
  time_t now_t;

  Serial.println(F("handleAlarms"));
  time_t tutc=time(nullptr);
  if(!tutc>ValidTime)
    return;

  t=localtime(&tutc);
  now_t=t->tm_hour*SECS_PER_HOUR + t->tm_min*SECS_PER_MIN;

  for(int i=0; i<ScheduleData.relaycount; i++)
  {
    PrgSchedule::Relay* r;
    r=&ScheduleData.relay[i];

    for(int j=0; j<r->programcount; j++)
    {
      program=&r->program[j];

      // check if there's something to trigger
      if(program->active && validDay(t->tm_wday, program->dayofweek))
      {
        start_t=program->starth*SECS_PER_HOUR + program->startm*SECS_PER_MIN;
        end_t=program->endh*SECS_PER_HOUR + program->endm*SECS_PER_MIN;
        if(firstTime)
        {
          // power on relay if it's the first start of the module
          if(!lampOn && start_t<=now_t && now_t<end_t)
            lampPowerOn();
        }
        else if(!lampOn && start_t==now_t) // "==" to preserve manual off
          lampPowerOn();
        else if(lampOn && end_t==now_t) // "==" to preserve manual on
          lampPowerOff();
      }
    } // for programcount
  } // for relaycount
}

void sendDataTS(bool startup)
{
  String s;

  Serial.println(F("sendDataTS"));
  s+=F("api_key=");
  s+=apiKey;
  s+=F("&field1=");
  s+=String(waterTermperature, 1);
  if(startup)
  {
    s+=F("&status=");
    s+=String(ESP.getFreeHeap())+" "+ESP.getResetInfo()+" "+ESP.getResetReason();
  }

  httpClient.setReuse(true);
  httpClient.begin(wifiClient, F("http://api.thingspeak.com/update"));
  httpClient.addHeader(F("Content-Type"), F("application/x-www-form-urlencoded"));
  int status=httpClient.POST(s);
  String payload=httpClient.getString(); // must be present for GET or POST to be executed
  httpClient.end();
  Serial.print(status);
  Serial.print(payload);
  Serial.println(F(" sendDataTS end"));
}

void setReadyForPowerOff()
{
  readyForPowerOff=true;
}

void setReadyForDataSend()
{
  readyForDataSend=true;
}

void setReadyForUpdate()
{
  readyForUpdate=true;
}

void setReadyForAlarmHandle()
{
  readyForAlarmHandle=true;
}

void readSensor()
{
  float t;
  time_t ts=time(nullptr);
  
  Serial.println(F("readSensor"));
  DS18B20.requestTemperatures(); 
  t=DS18B20.getTempC(DS18B20addr);
  waterTermperature=t;
  SetSensor(waterTermperature, formatTime(ts));

  // send temperature data to MQTT broker
  if(mqttEnabled)
  {
    String buf;
    const size_t bufsize = JSON_OBJECT_SIZE(3);
    StaticJsonDocument<bufsize> doc;
    String connTopic=mqttToplevel + "/status/temperature";

    doc["val"] = waterTermperature;
    doc["ts"] = (unsigned long long)ts*1000LL; // milliseconds
    doc["lc"] = doc["ts"];
    serializeJson(doc, buf);
    mqttClient.publish(connTopic.c_str(), buf.c_str());
  }
}

void handleConfig()
{
  String page;
  String message;
  MqttConfig mc;
  StaticIPConfig staticip;

  Serial.println(F("handleConfig"));
  if(server.hasArg("ok"))
  {
    // form was submitted, take action
    String opw=server.arg("oldpassword");
    String npw=server.arg("newpassword");
    String npw2=server.arg("newpassword2");
    String ntpsrv=server.arg("ntpsrv");
    String apikey=server.arg("apikey");
    String appname=server.arg("appname");
    bool invlogic=false;
    if(server.hasArg("invlogic"))
      invlogic=true;
    mc.toplevel=server.arg("mqtttoplevel");
    mc.server=server.arg("mqttsrv");
    mc.port=server.arg("mqttport").toInt();
    mc.username=server.arg("mqttusername");
    mc.password=server.arg("mqttpassword");
    staticip.ip=server.arg("staticip");
    staticip.dns=server.arg("staticdns");
    staticip.gw=server.arg("staticgw");
    staticip.subnet=server.arg("staticsubnet");

    if(opw.length()>0 && opw!=String(sysPassword))
      message="Error: wrong password!<br>";
    else if(npw!=npw2)
      message="Error: new passwords are different!<br>";
    else if(mc.server.length()>0 && mc.toplevel.length()==0)
      message="Error: you must specify the MQTT toplevel name!<br>";
    else
    {
      if(npw.length()>0)
        sysPassword=npw;
      timeServer=ntpsrv;
      apiKey=apikey;
      appName=appname;
      staticIP=staticip.ip;
      staticDNS=staticip.dns;
      staticGW=staticip.gw;
      staticSubnet=staticip.subnet;
      invertedLogic=invlogic;
      mqttServer=mc.server;
      mqttEnabled=(mqttServer.length()>0);
      mqttPort=mc.port;
      mqttUsername=mc.username;
      mqttPassword=mc.password;
      mqttToplevel=mc.toplevel;
      SetAppName(appName);
      saveConfig();

      message="Config saved, please reboot<br>";
    }
  } else {
    mc.server=mqttServer;
    mc.port=mqttPort;
    mc.username=mqttUsername;
    mc.password=mqttPassword;
    mc.toplevel=mqttToplevel;
    staticip.ip=staticIP;
    staticip.dns=staticDNS;
    staticip.gw=staticGW;
    staticip.subnet=staticSubnet;
  }

  ConfigPage(&page, appName, timeServer, apiKey, invertedLogic, staticip, mc, message);
  server.send(200, "text/html", page);
}

void loadConfig()
{
  File f;

  f=SPIFFS.open(configFilePath, "r");
  if(f)
  {
    const size_t bufsize = JSON_OBJECT_SIZE(4) +
                           JSON_OBJECT_SIZE(5) +
                           JSON_OBJECT_SIZE(7) +
                           f.size();
    DynamicJsonDocument doc(bufsize);
    DeserializationError error = deserializeJson(doc, f);
    if(!error)
    {
      // if a key is not found a NULL value is returned or 0 for a int data
      // can be tested with
      // JsonVariant password=doc["password"];
      // if(password.success())
      //   sysPassword=password.as<String>();
      sysPassword=doc["password"].as<String>();
      timeServer=doc["ntpsrv"].as<String>();
      apiKey=doc["tskey"].as<String>();
      appName=doc["appname"].as<String>();
      invertedLogic=doc["invlogic"].as<bool>();
      SetAppName(appName);

      JsonObject setpoint = doc["setpoint"];
      setPointValue=setpoint["value"].as<float>();
      setPointRange=setpoint["range"].as<float>();

      JsonObject staticip = doc["staticip"];
      staticIP=staticip["ip"].as<String>();
      staticDNS=staticip["dns"].as<String>();
      staticGW=staticip["gw"].as<String>();
      staticSubnet=staticip["subnet"].as<String>();

      JsonObject mqtt = doc["mqtt"];
      mqttToplevel=mqtt["toplevel"].as<String>();
      mqttServer=mqtt["server"].as<String>();
      mqttEnabled=(mqttServer.length()>0);
      mqttPort=mqtt["port"].as<int>();
      mqttUsername=mqtt["username"].as<String>();
      mqttPassword=mqtt["password"].as<String>();
    }

    f.close();
  }
}

void saveConfig()
{
  File f;

  const size_t bufsize = JSON_OBJECT_SIZE(4) +
                         JSON_OBJECT_SIZE(5) +
                         JSON_OBJECT_SIZE(7) +
                         50 + 50 + 100 + 50 +
                         16 + 16 + 16 + 16 +
                         50 + 100 + 50 + 50;
  DynamicJsonDocument doc(bufsize);

  doc["password"] = sysPassword; // 50 bytes
  doc["appname"] = appName; // 50 bytes
  doc["ntpsrv"] = timeServer; // 100 bytes
  doc["tskey"] = apiKey; // 50 bytes
  doc["invlogic"] = invertedLogic;

  JsonObject setpoint = doc.createNestedObject("setpoint");
  setpoint["value"] = setPointValue;
  setpoint["range"] = setPointRange;

  JsonObject staticip = doc.createNestedObject("staticip");
  staticip["ip"] = staticIP; // 16 bytes
  staticip["dns"] = staticDNS; // 16 bytes
  staticip["gw"] = staticGW; // 16 bytes
  staticip["subnet"] = staticSubnet; // 16 bytes

  JsonObject mqtt = doc.createNestedObject("mqtt");
  mqtt["toplevel"] = mqttToplevel; // 50 bytes
  mqtt["server"] = mqttServer; // 100 bytes
  mqtt["port"] = mqttPort;
  mqtt["username"] = mqttUsername; // 50 bytes
  mqtt["password"] = mqttPassword; // 50 bytes

  f=SPIFFS.open(configFilePath, "w");
  if(!f)
    Serial.println(F("Error opening config file"));
  else
  {
    serializeJsonPretty(doc, f);

    f.close();
  }
}

void MqttCallback(char* topic, byte* payload, unsigned int length)
{
  String commandTopic=mqttToplevel + "/on";

  if(commandTopic==topic)
  {
    int t;
    const size_t bufsize = JSON_OBJECT_SIZE(2) + length;
    DynamicJsonDocument doc(bufsize);
    DeserializationError error = deserializeJson(doc, payload);
    if(error)
      return;

    t=doc["val"].as<int>();
    if(t)
    {
      if(t<=60)
      {
        lampPowerOn();
        offTicker.once(t*60, setReadyForPowerOff);
      }
    }
    else
      lampPowerOff();
  }
}

bool MqttReconnect()
{
  int willQos=2;
  bool willRetain=true;
  // json format for message
  const char* willMessage="{\"val\": 0}";
  // if username is present then use authentication
  const char *user=(mqttUsername.length()>0 ? mqttUsername.c_str(): NULL);
  const char *pass=(mqttUsername.length()>0 ? mqttPassword.c_str(): NULL);
  String connTopic=mqttToplevel + "/connected";
  
  // use hostname as client id
  if(mqttClient.connect(Hostname.c_str(), user, pass, connTopic.c_str(), willQos, willRetain, willMessage))
  {
    // connected and fully operational. It's not true (sensors may be disconnected)
    // but fine for now
    String buf;
    const size_t bufsize = JSON_OBJECT_SIZE(2);
    StaticJsonDocument<bufsize> doc;

    doc["val"] = 2;
    doc["ts"] = time(nullptr)*1000LL;
    String commandTopic=mqttToplevel + "/on";

    serializeJson(doc, buf);
    mqttClient.publish(connTopic.c_str(), buf.c_str(), true);
    mqttClient.subscribe(commandTopic.c_str());
  }

  return mqttClient.connected();
}

// the setup routine runs once when you press reset:
void setup()
{
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  pinMode(lampPin, OUTPUT);
  pinMode(heaterPin, OUTPUT);

  // ESP.eraseConfig();
  EEPROM.begin(4); // used only for recovery mode, 4 minimum space reserved
  // check if we have to go to recovery mode
  bool recoveryMode=false;
  pinMode(ledPin, OUTPUT);
  {
    int r=EEPROM.read(0x00);
    if(r>=2)
    {
      recoveryMode=true;
      digitalWrite(ledPin, HIGH);
      EEPROM.write(0x00, 0);
    }
    else
      EEPROM.write(0x00, r+1);
    EEPROM.commit();
  }

  SPIFFS.begin();
  // load main configuration data
  loadConfig();

  // start with output powered off, must be done after loading configuration data
  // to know if it's operating in reverse logic
  lampPowerOff();
  heaterPowerOff();

  Hostname=appName+String(F("-"))+String(ESP.getChipId(), HEX);
  WiFi.hostname(Hostname.c_str());

  if(!recoveryMode)
  {
    Serial.println(F("Trying previous AP..."));
    // don't always save credential
    WiFi.persistent(false);
    WiFi.setAutoReconnect (true);
    WiFi.mode(WIFI_STA);
    // try the static ip if present
    if(staticIP.length()>0)
    {
      IPAddress ip;
      IPAddress dns;
      IPAddress gw;
      IPAddress subnet;
      bool ok=true;

      ok=ok && ip.fromString(staticIP);
      ok=ok && dns.fromString(staticDNS);
      ok=ok && gw.fromString(staticGW);
      ok=ok && subnet.fromString(staticSubnet);
      if(ok)
      {
        WiFi.config(ip, dns, gw, subnet);
        WiFi.begin();
        // wait for connection
        for(int i=0; i<20 && WiFi.status()!=WL_CONNECTED; i++)
          delay(500);
      }
    }

    // fallback to DHCP if static ip failed
    if(WiFi.status()!=WL_CONNECTED)
    {
      WiFi.begin();
      // wait for connection
      for(int i=0; i<20 && WiFi.status()!=WL_CONNECTED; i++)
        delay(500);
    }

    if(WiFi.status()!=WL_CONNECTED)
      Serial.println(F("Failed to connect to saved AP, start a private AP"));
    else
    {
      Serial.print(F("Connected to previous AP with IP: "));
      Serial.println(WiFi.localIP());
      subnetMask=WiFi.subnetMask();
    }
  }

  if (recoveryMode || WiFi.status()!=WL_CONNECTED)
  {
    String apname=appName+String(F("AP-"))+String(ESP.getChipId(), HEX);
    IPAddress subnet(255, 255, 255, 0);
    subnetMask=subnet;

    delay(1000);
    standaloneMode=true;
    if(recoveryMode)
    {
      WiFi.setOutputPower(1);
      // disable authentication
      sysPassword="";
      apname=String(F("RecoveryAP-"))+String(ESP.getChipId(), HEX);
    }
    // start an AP
    WiFi.mode(WIFI_AP_STA);
    WiFi.softAPConfig(apIP, apIP, subnet);
    if(sysPassword.length()>0)
      WiFi.softAP(apname.c_str(), sysPassword.c_str()); 
    else
      WiFi.softAP(apname.c_str()); 
    Serial.print("Access Point started at: ");
    Serial.println(WiFi.softAPIP());
    /* Setup the DNS server redirecting all the domains to the apIP */  
    dnsServer.setErrorReplyCode(DNSReplyCode::NoError);
    dnsServer.start(DNS_PORT, "*", apIP);
  }
  else
    standaloneMode=false;

  // disable auto connection when ESP start after power on
  WiFi.setAutoConnect(false);

  Serial.println(F("Start main tasks..."));
  server.begin();

  if(sysPassword.length()>0)
    httpUpdater.setup(&server, webUsername.c_str(), sysPassword.c_str());
  else
    httpUpdater.setup(&server);

  server.on("/", HandleRoot);
  server.onNotFound(HandleNotFound);

  loadProgramData();

  if(!standaloneMode)
  {
    if(mqttEnabled)
    {
      mqttClient.setServer(mqttServer.c_str(), mqttPort);
      mqttClient.setCallback(MqttCallback);
    }

    readNTPtime();
    ntpTicker.attach(SECS_PER_DAY, readNTPtime);
  }

  DS18B20.begin();
  if(!DS18B20.getAddress(DS18B20addr, 0))
    Serial.println(F("Unable to find address for Device 0"));
  else
    // set the resolution to 9 bit per device
    DS18B20.setResolution(DS18B20addr, TEMPERATURE_PRECISION);

  readSensor();
  sensorTicker.attach(UPDATE_INTERVAL_SECS, setReadyForUpdate);

  
  if(!standaloneMode)
  {
    handleAlarms(true);
    alarmTicker.attach(ALARM_CHECK_SECS, setReadyForAlarmHandle);

    if(apiKey.length()>0)
    {
      sendDataTS(true);
      sendTicker.attach(SECS_PER_HOUR, setReadyForDataSend);
    }
  }
}

// the loop routine runs over and over again forever:
void loop()
{
  if(recoveryCheck)
  {
    if(millis() > 10000)
    {
      EEPROM.write(0x00, 0);
      EEPROM.end();
      recoveryCheck=false;
      digitalWrite(ledPin, HIGH);
    }
  }

  //DNS
  if(standaloneMode)
    dnsServer.processNextRequest();
  //HTTP
  server.handleClient();

  if(!standaloneMode && mqttEnabled && !mqttClient.connected()) {
    long now = millis();
    if (now - mqttLastReconnectAttempt > 5000) {
      mqttLastReconnectAttempt = now;
      // test connection
      MqttTestClient.setTimeout(1000);
      if(MqttTestClient.connect(mqttServer.c_str(), mqttPort))
      {
        // if the connection is ok use the real Mqtt client
        MqttTestClient.stop();
        if (MqttReconnect())
          mqttLastReconnectAttempt = 0;
      }
    }
  } else
    mqttClient.loop(); // Client connected

  if(startTime.length()==0)
  {
    if(time(nullptr)>ValidTime)
    {
      handleAlarms(true);
      startTime=formatTime(time(nullptr));
    }
  }

  // handle here all the tickers to avoid interrupt conflict and leave time for WiFi tasks
  if (readyForUpdate)
  {
    readSensor();
    if(waterTermperature < setPointValue-setPointRange)
      heaterPowerOn();
    else if(waterTermperature > setPointValue+setPointRange)
      heaterPowerOff();
    readyForUpdate = false;
    delay(1);
  }

  if(readyForDataSend)
  {
    sendDataTS(false);
    readyForDataSend=false;
    delay(1);
  }

  if(readyForAlarmHandle)
  {
    handleAlarms(false);
    readyForAlarmHandle=false;
    delay(1);
  }

  if(readyForPowerOff)
  {
    lampPowerOff();
    readyForPowerOff=false;
    delay(1);
  }

  delay(10);
}
