DAquariumMCU
============

Control the aquarium lamp with an MCU

The board can control the lamp of your fish tank based on scheduling times.
You can program when the light is switched on or off based on time and week days.
You can manually set the state "on" or "off", when putting to "on" you choose
for how long it will be "on" based on predefined values (in minutes) and if
current or subsequent schedule is honored. The scheduling can be active or
inactive without having to remove the scheduling set before.
The board can read temperature of the water with a waterproof sensor plugged and 
with the value can trigger a relay based on a defined set point and a range.
All settings are saved in the internal file system (SPIFFS) in JSON format
[ArduinoJson](https://bblanchon.github.io/ArduinoJson/assistant/).
The board is fully usable without connection to other machine on private
or public network.
It can connect to an MQTT broker to send information:
  - water temperature
  - light status
  - if scheduling is active
 

# Hardware


[Lolin v3 nodeMcu board](https://it.aliexpress.com/item/New-Wireless-module-CH340-NodeMcu-V3-Lua-WIFI-Internet-of-Things-development-board-based-ESP8266/32549862093.html)

![lolin-v3](images/lolin-v3.jpg)


[Relay board](https://it.aliexpress.com/item/Hot-Sale-5V-1-One-Channel-Relay-Module-Low-level-for-SCM-Household-Appliance-Control-for/32316656858.html)

![relay](images/relay.jpg)


[DS18b20](https://it.aliexpress.com/item/Stainless-steel-package-Waterproof-DS18b20-temperature-probe-temperature-sensor-18B20-For-Arduino/1738746636.html)

![DS18b20](images/DS18b20.jpg)


# WiFi connection

If the board wasn't configured for WiFi access before or can't connect to a
saved network then it starts an Access Point with name DAquarium-##, where ## is
a unique identifier (the chip ID).
The AP has password "password". You can connect to the AP and navigate to
http://192.168.1.1/ (see MCU Web Server).


# MCU Web Server

The board always provides a minimal web interface to configure Wifi settings,
get status and command the board.
The server provides those functions:
  1.  **wifi configuration**
  2.  **board status**
  3.  **firmware update**
  4.  reboot
  5.  reset to factory settings
  6.  **"about" informations**
  7.  **basic settings**
  8.  **set point settings**
  9.  **scheduling times**
  10.  **manual activation**
  11. download configuration data

Some functions are implemented using web pages and every page, not including the
status page, has a link or a button to return to the board status page.
The pages, with the corresponding relative web address, are the ones in
**bold**.
"reboot" and "reset to factoy settings" are implemented using a confirmation
dialog.
"download configuration data" sends data directly to the user.


## Wifi Configuration

The page provides a list of available Wifi networks; the user can select from
the list or type a name.

In a second field the user enter the WiFi password.

A button allows to begin a WPS connection.

The page shows the name of the current connected network with the signal
strength and let the user to disconnect from it.


## Board status

The page shows the current status of the board:
  1. actual date and time
  2. current water temperature
  3. last time the sensor was read
  4. if is running a scheduled program (the light is on)
  5. scheduling data

On this page the user can go to the other pages and activate "reboot", "reset"
and "download" functions.


## Firmware update

This function (and page) is the one provided by the esp8266 SDK.


## Reboot

After a javascript confirmation dialog, reboots the board.


## Reset to factory settings

After a javascript confirmation dialog, deletes the files with settings and
disconnect from WiFi.


## "about" informations

The page shows information about:
  - chip ID
  - cpu frequency
  - last reset reason
  - start date and time or uptime
  - compilation date and time


## Basic settings

The page allows to change the current password for HTTP authentication,
if left blank the authentication is disabled. Three fields are provided:
current password, new password and new password verification.

Another field is for the NTP server and one for the ThingSpeak API key.

Three fields are for configuring the MQTT broker: address, username, password.
If address is left blank no data is sent to MQTT broker. A toplevel domain must
be specified if using the MQTT server, defaults to "daquarium". The toplevel
can also be configured as a series of topic path, e.g. myhome/daquarium.

In this page the user can set date and time if NTP server is not present.

All settings are applied after a restart.

JSON example:
```
{
  "password": "http password",
  "ntpsrv": "pool.ntp.org",
  "tskey": "ABCDEFGHIJKLMNOP",
  "appname": "DAquarium",
  "setpoint": {
            "value": 24.0,
            "range": 0.3,
          },
  "mqtt": {
            "server": "localhost",
            "port": 1883,
            "username": "mqttuser",
            "password": "mqttpassword",
            "toplevel": "daquarium"
          }
}
```

## Set point settings

The page provide a form to change the set point value and range.
The settings are saved in JSON format in the same structure of the basic
settings.

## Scheduling times

Five scheduling set are provided. For every schedule is possible to set the on
and off time, the days of week when it will be used and if it's active.

JSON example:
```
{
  "relaycount": 1,
  "relay":
  [
    {
      "programcount": 2,
      "program":
      [
        {
          "active": true,
          "starth": 11,
          "startm":12,
          "endh":13,
          "endm":14,
          "dayofweek": 233
        },

        {
          "active": false,
          "starth": 10,
          "startm": 11,
          "endh": 12,
          "endm": 13,
          "dayofweek": 212
        }
      ]
    }
  ]
}
```

## Manual activation

The user can change the status of the light **On** and **Off** and tell to
the system if the next or current scheduling has to be honored.

## Download configuration data

Send data directly to the user, the three different files can be download with
"?download=" after the request and the number represeting the file:
  1. config file
  2. scheduleing file

# MQTT connection

The topics are contructed based on [MQTT smarthome Architecture](https://github.com/mqtt-smarthome/mqtt-smarthome/blob/master/Architecture.md)
and are prefixed by the toplevel name as specified in configuration page.
QoS setting is always 0 (no guaranteed delivery, no duplicates possible) or
2 (guaranteed delivery, no duplicates possible).

Topic "\<toplevel\>/connected" with value:
  - 0 disconnected
  - 1 connected but hardware error
  - 2 connected and fully operational

It should ensure that this is set to 0 using MQTT's Last Will and Testament
functionality upon a disconnect.

The values of the sensors are expressed with a Json string:
    - val - the value of the sensor
    - ts - the timestamp when the value was obtained. Milliseconds since Epoch.
    - lc - the timestamp when the value last changed. Milliseconds since Epoch.
The timestamps are adjusted to local time.

Example:
  {
	"val": 17.9,
	"ts": 1421792677000,
	"lc": 1421792677000
  }

Topic name for temperature sensor DS18b20 is "\<toplevel\>/status/temperature".

The topic "\<toplevel\>/status/active" is published with the status of the
light: {"val": active, "ts": timestamp}.
Where active is 1 for on and 0 for off, timestamp is the milliseconds since
Epoch when the lamp switched on or off.

The light can be activated with the topic "\<toplevel\>/on" and message
{"val": command} where command is 1 to turn on and 0 to turn off: the values
are of type json boolean. A duration can be provided with name "duration" and
the value can be one of a predefined set: 5, 10, 15, 20 minutes. If this key
isn't present it defaults to 10 minutes.

All payload is in Json format, when there is only a value to send, the json
object is {"val": value}.


# Recovery mode

The board provides the ability to start the AP and web interface without using a
password in case the user forget the password.
Switch on and off (unplug it) the board three times with a maximum of 10 seconds
beetween every switching.
For security reason, the WiFi power in recovery mode is lowered so the range is 
very limited, stay close to the board.
