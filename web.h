/*
 Copyright 2016 - Domenico Ferrari <domfe@tiscali.it>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __WEB_H__
#define __WEB_H__

#ifndef __xtensa__
  #include <string>
  #define String std::string
  #define remove erase
  typedef unsigned char byte;
#else
  #include <Arduino.h>
  #include <WString.h>
#endif
#include "prgschedule.h"

class MqttConfig
{
public:
  String server;
  int port;
  String username;
  String password;
  String toplevel;
};

class StaticIPConfig
{
public:
  String ip;
  String dns;
  String gw;
  String subnet;
};

extern void SetAppName(String appname);
extern void SetSensor(float t, String lastread);
extern void ConfigPage(String* p, String appname, String ntpsrv, String apikey, bool invlogic, StaticIPConfig staticip, MqttConfig mqttconfig, String message);
extern void CommandPage(String* s);
extern void ProgramPage(String *p, PrgSchedule *schedule);
extern void StatusPage(String *p, String timestr, String setpoint, String range, bool relaystate, PrgSchedule *schedule);
extern void SetPointPage(String *p, String setpoint, String range);
extern void AboutPage(String *p, String version, String startTime, String chipid, int cpufreq, String resetreason, String ssid, int rssi, String boardType);
extern void WiFiPage(String *p, String ssid, String pwd, String network[], int networknum);
extern void RefreshPage(String *p, int refreshtime, String message, String url);

#endif
