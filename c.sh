#!/bin/bash

if [ "$1" == "-t" ] ; then
  gcc webtest.cpp web.cpp -DDEBUG_OFFLINE -o webtest -lstdc++
else

  if [ "$1" == "-d1" ] ; then
    BOARD=d1_mini
  else
    BOARD=nodemcuv2
  fi

  if [ "$OSTYPE" == "linux-gnu" ] ; then
    arduino --board esp8266:esp8266:$BOARD:FlashSize=4M3M --pref build.path=$PWD/cache --verify $PWD/daquariumMCU.ino
  else
    mkdir -p cache
    arduino-builder -compile \
      -hardware ../../packages -hardware ../../../hardware \
      -tools ../../../tools-builder -tools ../../../hardware/tools/avr -tools ../../packages \
      -built-in-libraries ../../../libraries -libraries ../libraries \
      -fqbn=esp8266:esp8266:$BOARD:FlashSize=4M3M \
      -build-path $PWD/cache \
      -prefs=build.warn_data_percentage=75 \
      -prefs=runtime.tools.xtensa-lx106-elf-gcc.path=../../packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2 \
      -prefs=compiler.sdk.path=../../packages/esp8266/hardware/esp8266/2.3.0/tools/sdk \
      ./daquariumMCU.ino
  fi
fi

