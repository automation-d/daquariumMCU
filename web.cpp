/*
 Copyright 2016-2019 - Domenico Ferrari <domfe@tiscali.it>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "web.h"

#ifndef __xtensa__
  #define F(s)  s

  static String intToString(int v)
  {
    String s;
    char conv[20];

    sprintf(conv, "%d", v);
    s=conv;

    return s;
  }

  static String digitToString(int v)
  {
    String s;
    char conv[20];

    sprintf(conv, "%02d", v);
    s=conv;

    return s;
  }

  static String floatToString(float v)
  {
    String s;
    char conv[20];

    sprintf(conv, "%.2f", v);
    s=conv;

    return s;
  }
#else
  #define intToString(v) (v)

  static String digitToString(int v)
  {
    String s;

    if(v<10)
      s="0";
    s+=v;

    return s;
  }

  static String floatToString(float v)
  {
    String s(v, 2);

    return s;
  }
#endif

static String appName="";

static float Temperature=0.0f;
static String LastRead="";

static String R1text("Luce");

static String empty="";

void closeTag(String *p, String tag)
{
  *p+=F("</");
  *p+=tag;
  *p+=F(">");
}

void oScript(String *p)
{
  *p+=F("<script>");
}
#define cScript(p)       closeTag(p, F("script"))

void PageStart(String *p, String title)
{
  *p+=F("<!DOCTYPE HTML>");
  *p+=F("<html>");
  *p+=F("<head>");

  *p+=F("<meta charset=\"UTF-8\">");
  *p+=F("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");

  *p+=F("<style>");
  *p+=F("body { font-family: Arial; font-size: 10pt; }");
  *p+=F("a, a:visited { color: gray; margin: 5px; }");
  *p+=F("input, select { margin: 2px; }");
  *p+=F("label { font-weight: bold; margin-right: 3px; }");
  *p+=F("label.radio, label.checkbox { font-weight: normal; }");
  *p+=F("div.well { padding: 5px; background-color: #eaeaea; margin: 4px; }");
  *p+=F(".container { padding-right: 5px; padding-left: 5px }");
  *p+=F(".nowrap { white-space: nowrap; }");
  *p+=F(".monospace { font-family: monospace; }");
  *p+=F("</style>");

  *p+=F("<title>");
  *p+=appName;
  *p+=F(" - ");
  *p+=title;
  *p+=F("</title>");
  *p+=F("</head>");
  *p+=F("<body>");
}

void PageEnd(String *p)
{
  *p+=F("</body>");
  *p+=F("</html>");
}

void oForm(String *p, String action)
{
  *p+=F("<form method=\"post\" class=\"container\" action=\"");
  *p+=action;
  *p+=F("\">");
}
#define cForm(p)       closeTag(p, F("form"))

void btnSubmit(String *p, String text, String name="", String value="")
{
  *p+=F("<button type=\"submit\"");

  if(name.length()>0)
  {
    *p+=F(" name=\"");
    *p+=name;
    *p+=F("\"");
  }

  if(value.length()>0)
  {
    *p+=F(" value=\"");
    *p+=value;
    *p+=F("\"");
  }

  *p+=">";
  *p+=text;
  *p+=F("</button>");
}

void btnOnClick(String *p, String text, String onclick)
{
  *p+=F("<button type=\"button\" onclick=\"");
  *p+=onclick;
  *p+=F("\">");
  *p+=text;
  *p+=F("</button>");
}

#define btnHome(p)    btnSubmit(p, F("Home"))

void btnOk(String *p)
{
  *p+=F("<button type=\"submit\" name=\"ok\" value=\"1\">OK</button>");
}

void pSpace(String *p)
{
  *p+=F("<p></p>");
}

void Label(String *p, String text)
{
  *p+=F("<label>");
  *p+=text;
  *p+=F("</label>");
}

void Option(String *p, String value, String text)
{
  *p+=F("<option value=\"");
  *p+=value;
  *p+=F("\">");
  *p+=text;
  *p+=F("</option>");
}

void Br(String *p)
{
  *p+=F("<br>");
}

void Radio(String *p, String name, String value, bool checked, String text)
{
  *p+=F("<label class=\"radio\"><input type=\"radio\" name=\"");
  *p+=name;
  *p+=F("\" value=\"");
  *p+=value;
  *p+=F("\"");
  if(checked)
    *p+=F(" checked");
  
  *p+=F(">");
  *p+=text;
  *p+=F("</label>");
}

void Checkbox(String *p, String name, bool checked, String text)
{
  *p+=F("<label class=\"checkbox\"><input type=\"checkbox\" name=\"");
  *p+=name;
  *p+=F("\"");
  if(checked)
    *p+=F(" checked");

  *p+=F(">");
  *p+=text;
  *p+=F("</label>");
}

void oDiv(String *p, String cssclass="", String id="")
{
  *p+=F("<div");

  if(cssclass.length()>0)
  {
    *p+=F(" class=\"");
    *p+=cssclass;
    *p+=F("\"");
  }

  if(id.length()>0)
  {
    *p+=F(" id=\"");
    *p+=id;
    *p+=F("\"");
  }

  *p+=F(">");
}

#define cDiv(p)        closeTag(p, F("div"))
#define oContainer(p)  oDiv(p, F("container"))
#define cContainer(p)  cDiv(p)
#define oGroup(p)      oDiv(p, F("well"))
#define cGroup(p)      cDiv(p)

void oSelect(String *p, String name)
{
  *p+=F("<select name=\"");
  *p+=name;
  *p+=F("\">");
}
#define cSelect(p)        closeTag(p, F("select"))

void Bold(String *p, String text)
{
  *p+=F("<b>");
  *p+=text;
  *p+=F("</b>");
}

void Hidden(String *p, String name, String value)
{
  *p+=F("<input type=\"hidden\" name=\"");
  *p+=name;
  *p+=F("\" value=\"");
  *p+=value;
  *p+=F("\">");
}

void InputText(String *p, String name, String value="", String maxlength="")
{
  *p+=F("<input type=\"text\" name=\"");
  *p+=name;
  *p+=F("\" value=\"");
  *p+=value;
  if(maxlength.length()>0)
  {
    *p+=F("\" maxlength=\"");
    *p+=maxlength;
  }
  *p+=F("\">");
}

void InputPassword(String *p, String name, String value="", String maxlength="")
{
  *p+=F("<input type=\"password\" name=\"");
  *p+=name;
  *p+=F("\" value=\"");
  *p+=value;
  if(maxlength.length()>0)
  {
    *p+=F("\" maxlength=\"");
    *p+=maxlength;
  }
  *p+=F("\">");
}

void InputNumber(String *p, String name, String value="", String min="0", String max="100", String step="1", String maxlength="2", String isize="2")
{
  *p+=F("<input type=\"text\" name=\"");
  *p+=name;
  *p+=F("\" value=\"");
  *p+=value;
  *p+=F("\" min=\"");
  *p+=min;
  *p+=F("\" max=\"");
  *p+=max;
  *p+=F("\" step=\"");
  *p+=step;
  *p+=F("\" maxlength=\"");
  *p+=maxlength;
  *p+=F("\" size=\"");
  *p+=isize;
  *p+=F("\">");
}

void Link(String *p, String text, String href, String onclick="")
{
  *p+=F("<a href=\"");
  *p+=href;
  if(onclick.length()>0)
  {
    *p+=F("\" onclick=\"");
    *p+=onclick;
  }
  *p+=F("\">");
  *p+=text;
  *p+=F("</a>");
}

void ConfigPage(String* p, String appname, String ntpsrv, String apikey, bool invlogic, StaticIPConfig staticip, MqttConfig mqttconfig, String message)
{
  PageStart(p, F("Config"));

  oForm(p, F("?"));
  btnHome(p);
  cForm(p);

  pSpace(p);

  oForm(p, F("?config=1"));

  oGroup(p);
  Label(p, F("Application name:"));
  InputText(p, F("appname"), appname);
  Br(p);
  Br(p);
  Label(p, F("Old password:"));
  InputPassword(p, F("oldpassword"));
  Br(p);
  Label(p, F("New password:"));
  InputPassword(p, F("newpassword"));
  Br(p);
  Label(p, F("Verify password:"));
  InputPassword(p, F("newpassword2"));
  Br(p);
  Br(p);
  Label(p, F("NTP server:"));
  InputText(p, F("ntpsrv"), ntpsrv);
  btnOnClick(p, F("Sync time"), F("location.href='?synctime=1';"));
  Br(p);
  Br(p);
  Label(p, F("Thingspeak API key:"));
  InputText(p, F("apikey"), apikey);
  Br(p);
  Br(p);
  Label(p, F("Relay logic:"));
  Checkbox(p, F("invlogic"), invlogic, F("inverted (LOW for on)"));
  Br(p);
  cGroup(p);

  Br(p);

  oGroup(p);
  Label(p, F("Static IP"));
  Br(p);
  Label(p, F("IP"));
  InputText(p, F("staticip"), staticip.ip);
  Br(p);
  Label(p, F("DNS Server"));
  InputText(p, F("staticdns"), staticip.dns);
  Br(p);
  Label(p, F("Gateway"));
  InputText(p, F("staticgw"), staticip.gw);
  Br(p);
  Label(p, F("Subnet mask"));
  InputText(p, F("staticsubnet"), staticip.subnet);
  cGroup(p);

  Br(p);

  oGroup(p);
  Label(p, F("MQTT"));
  Br(p);
  Label(p, F("Toplevel name:"));
  InputText(p, F("mqtttoplevel"), mqttconfig.toplevel);
  Br(p);
  Label(p, F("Address:"));
  InputText(p, F("mqttsrv"), mqttconfig.server);
  Br(p);
  Label(p, F("Port:"));
  InputText(p, F("mqttport"), String()+intToString(mqttconfig.port));
  Br(p);
  Label(p, F("Username:"));
  InputText(p, F("mqttusername"), mqttconfig.username);
  Br(p);
  Label(p, F("Password:"));
  InputPassword(p, F("mqttpassword"), mqttconfig.password);
  Br(p);
  cGroup(p);


  if(message.length()>0)
  {
    oGroup(p);
    Label(p, message);
    cGroup(p);
  }

  Br(p);
  btnOk(p);
  cForm(p);

  PageEnd(p);
}

void CommandPage(String *p)
{
  PageStart(p, F("Command"));

  oForm(p, F("?"));
  btnHome(p);
  cForm(p);

  pSpace(p);

  oForm(p, F("?command=1"));

  oGroup(p);
  Label(p, R1text);
  oDiv(p);
  Label(p, F("Time:"));
  oSelect(p, F("r1time"));
    Option(p, F("5"), F("5 min"));
    Option(p, F("10"), F("10 min"));
    Option(p, F("15"), F("15 min"));
    Option(p, F("30"), F("30 min"));
  cSelect(p);
  cDiv(p);
  oDiv(p);
  Radio(p, F("r1radio"), F("r1on"), false, F("On"));
  Radio(p, F("r1radio"), F("r1off"), false, F("Off"));
  Radio(p, F("r1radio"), F("r1unchanged"), true, F("Unchanged"));
  cDiv(p);
  cGroup(p);

  btnOk(p);
  cForm(p);

  PageEnd(p);
}

void ProgramPageRelay(String *p, String title, int rnum, PrgSchedule::Relay *relay)
{
  PrgSchedule::Relay::ProgramType *program;
  int i;
  String num;
  String name;

  PageStart(p, title);

  oForm(p, F("?"));
  btnHome(p);
  cForm(p);

  pSpace(p);

  oForm(p, String(F("?program=")) + intToString(rnum+1));

  oGroup(p);
  Label(p, title);
  oDiv(p);
  for(i=0; i<relay->programcount; i++)
  {
    program=&relay->program[i];
    name=F("p");
    name+=intToString(i);
    name+=F("r");
    name+=intToString(rnum);

    if(i>0)
      pSpace(p);
    Hidden(p, name, "1");
    oDiv(p);
    num=intToString(program->starth);
    InputNumber(p, name + F("starth"), num, F("0"), F("23"));
    Label(p, F(":"));
    num=intToString(program->startm);
    InputNumber(p, name + F("startm"), num, F("0"), F("59"));
    Label(p, F("&nbsp;-"));
    num=intToString(program->endh);
    InputNumber(p, name + F("endh"), num, F("0"), F("23"));
    Label(p, F(":"));
    num=intToString(program->endm);
    InputNumber(p, name + F("endm"), num, F("0"), F("59"));
    *p+=F("&nbsp;");
    cDiv(p);

    oDiv(p);
    Checkbox(p, name + F("active"), program->active, F("A"));
    *p+=F("&nbsp;");
    Checkbox(p, name + F("day1"), program->dayofweek & Monday, F("M"));
    Checkbox(p, name + F("day2"), program->dayofweek & Tuesday, F("T"));
    Checkbox(p, name + F("day3"), program->dayofweek & Wednesday, F("W"));
    Checkbox(p, name + F("day4"), program->dayofweek & Thursday, F("T"));
    Checkbox(p, name + F("day5"), program->dayofweek & Friday, F("F"));
    Checkbox(p, name + F("day6"), program->dayofweek & Saturday, F("S"));
    Checkbox(p, name + F("day7"), program->dayofweek & Sunday, F("S"));
    cDiv(p);
  }
  cDiv(p);
  cGroup(p);

  btnOk(p);

  cForm(p);

  PageEnd(p);
}

void ProgramPage(String *p, PrgSchedule *schedule)
{
  ProgramPageRelay(p, R1text, 0, &schedule->relay[0]);
}

static void StatusPageRelay(String *p, String title, PrgSchedule::Relay *relay)
{
  PrgSchedule::Relay::ProgramType *program;
  int i;

  oGroup(p);
  Label(p, title);
  oDiv(p, F("nowrap"));
  for(i=0; i<relay->programcount; i++)
  {
    program=&relay->program[i];
    *p+=intToString(program->starth);
    *p+=":";
    *p+=digitToString(program->startm);
    Label(p, F(" -"));
    *p+=intToString(program->endh);
    *p+=F(":");
    *p+=digitToString(program->endm);
    *p+=F(" ");
    if(program->active)
      Bold(p, F("A"));
    else
      *p+=F("X");
    *p+=F("  ");
    if(program->dayofweek & Monday)
      Bold(p, F("M"));
    else
      *p+=F("M");
    *p+=F(" ");
    if(program->dayofweek & Tuesday)
      Bold(p, F("T"));
    else
      *p+=F("T");
    *p+=F(" ");
    if(program->dayofweek & Wednesday)
      Bold(p, F("W"));
    else
      *p+=F("W");
    *p+=F(" ");
    if(program->dayofweek & Thursday)
      Bold(p, F("T"));
    else
      *p+=F("T");
    *p+=F(" ");
    if(program->dayofweek & Friday)
      Bold(p, F("F"));
    else
      *p+=F("F");
    *p+=F(" ");
    if(program->dayofweek & Saturday)
      Bold(p, F("S"));
    else
      *p+=F("S");
    *p+=F(" ");
    if(program->dayofweek & Sunday)
      Bold(p, F("S"));
    else
      *p+=F("S");
    Br(p);
  }
  cDiv(p);
  cGroup(p);
}

void SetSensor(float t, String lastread)
{
  Temperature=t;
  LastRead=lastread;
}

void SetAppName(String appname)
{
  appName=appname;
}

void WiFiPage(String *p, String ssid, String pwd, String network[], int networknum)
{
  PageStart(p, F("WiFi"));

  oForm(p, F("?"));
  btnHome(p);
  cForm(p);

  pSpace(p);
  Link(p, F("WiFi disconnect"),  F("#"), F("if(confirm('Are you sure you want to disconnect from WiFi network?')) location.href='?wifi=1&disconnect=1';"));
  Br(p);
  Link(p, F("WPS Connection"),  F("#"), F("if(confirm('Press WPS button on your router')) location.href='?wifi=1&wps=1';"));
  pSpace(p);

  oForm(p, F("?wifi=1"));

  oGroup(p);
  for(int i=0; i<networknum; i++)
  {
    Link(p, network[i], F("#"), F("document.getElementsByName('ssid')[0].value=this.innerText; document.getElementsByName('password')[0].value=''"));
    Br(p);
  }
  cGroup(p);

  Br(p);

  oGroup(p);
  Label(p, F("WiFi SSID:"));
  InputText(p, F("ssid"), ssid);
  Br(p);
  Label(p, F("Password:"));
  InputText(p, F("password"), pwd);
  cGroup(p);

  Br(p);
  btnOk(p);
  cForm(p);

  PageEnd(p);
}

void SetPointPage(String *p, String setpoint, String range)
{
  PageStart(p, F("Set point"));

  oForm(p, F("?"));
  btnHome(p);
  cForm(p);

  pSpace(p);

  oForm(p, F("?setpoint=1"));

  oDiv(p);
  Label(p, F("Set point:"));
  InputNumber(p, F("value"), setpoint, F("15"), F("32"), F("0.1"), F("5"), F("5"));
  Br(p);
  Label(p, F("Temperature range:"));
  *p+=F("&plusmn;");
  InputNumber(p, F("range"), range, F("0"), F("3"), F("0.1"), F("5"), F("5"));
  cDiv(p);

  btnOk(p);

  cForm(p);

  PageEnd(p);
}

void StatusPage(String *p, String timestr, String setpoint, String range, bool relaystate, PrgSchedule *schedule)
{
  PageStart(p, F("Status"));

  pSpace(p);
  oContainer(p);
  Link(p, R1text, F("?program=1"));
  Br(p);
  Link(p, F("Config"), F("?config=1"));
  Link(p, F("Set point"), F("?setpoint=1"));
  Link(p, F("Download"), F("?download=1"));
  Link(p, F("Reboot"), F("#"), F("if(confirm('Are you sure you want to reboot?')) location.href='?reboot=1';"));
  Link(p, F("Update"), F("update"));
  Link(p, F("WiFi"),  F("?wifi=1"));
  Link(p, F("Reset"),  F("#"), F("if(confirm('Are you sure you want to reset to factory settings?')) location.href='?reset=1';"));
  Br(p);
  Link(p, F("About"), F("?about=1"));
  cContainer(p);

  pSpace(p);
  oForm(p, F("?"));
  btnSubmit(p, F("Refresh"));
  btnSubmit(p, F("Manual"), F("command"), F("1"));
  cForm(p);
  pSpace(p);
  Br(p);

  oContainer(p);
  Label(p, timestr);
  cContainer(p);
  Br(p);

  oContainer(p);
  Label(p, F("Water temperature:"));
  *p+=floatToString(Temperature);
  *p+=F(" &deg;C");
  Br(p);
  Label(p, F("Last read:"));
  *p+=LastRead;
  Br(p);
  Label(p, F("Set point:"));
  *p+=setpoint;
  *p+=F(" (&plusmn;");
  *p+=range;
  *p+=F(")");
  cContainer(p);
  Br(p);

  oContainer(p);
  Label(p, R1text+F(":"));
  *p+=(relaystate ? F("on"): F("off"));
  cContainer(p);
  Br(p);

  oContainer(p);
  StatusPageRelay(p, R1text, &schedule->relay[0]);
  cContainer(p);

  PageEnd(p);
}

void AboutPage(String *p, String version, String startTime, String chipid, int cpufreq, String resetreason, String ssid, int rssi, String boardType)
{
  int rssip;

  PageStart(p, F("About"));

  oForm(p, F("?"));
  btnHome(p);
  cForm(p);

  pSpace(p);
  Br(p);
  oContainer(p);
  *p+=F("Board type: ");
  *p+=boardType;
  Br(p);  
  *p+=F("Chip ID: ");
  *p+=chipid;
  Br(p);
  *p+=F("CPU Freq: ");
  *p+=intToString(cpufreq);
  *p+=F(" MHz");
  Br(p);
  *p+=F("WiFi AP: ");
  *p+=ssid;
  Br(p);
  *p+=F("WiFi signal strength: ");
  *p+=intToString(rssi);
  *p+=F(" db (");
  if(rssi<=-100)
    rssip=0;
  else if(rssi>=-50)
    rssip=100;
  else
    rssip=2*(rssi+100);
  *p+=intToString(rssip);
  *p+=F("%)");
  Br(p);
  Br(p);
  *p+=F("Last reset reason:");
  Br(p);
  *p+=F("<pre>");
  *p+=resetreason;
  *p+=F("</pre>");
  cContainer(p);
  Br(p);

  oContainer(p);
  *p+=F("Started on ");
  *p+=startTime;
  Br(p);
  *p+=version;
  cContainer(p);
  Br(p);

  PageEnd(p);
}

void RefreshPage(String *p, int refreshtime, String message, String url)
{
  *p+=F("<!DOCTYPE HTML>");
  *p+=F("<html>");
  *p+=F("<head>");
  *p+=F("<meta charset=\"UTF-8\">");
  *p+=F("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
  *p+=F("<meta http-equiv=\"refresh\" content=\"");
  *p+=intToString(refreshtime);
  *p+=F("; url=");
  *p+=url;
  *p+=F("\">");
  *p+=F("<style>");
  *p+=F("body { font-family: Arial; font-size: 10pt; }");
  *p+=F("</style>");
  *p+=F("<title>");
  *p+=appName;
  *p+=F("</title>");
  *p+=F("</head>");
  *p+=F("<body>");
  *p+=message;
  *p+=F("</body>");
  *p+=F("</html>");
}
